import fastify, { FastifyError, FastifyReply, FastifyRequest } from 'fastify'
import fastifyCors from 'fastify-cors'
import config from './config'
import withErrors from './errors'
import movieRoutes from './movieRoutes'

const port = config.app.port

const app = fastify()

app.register(fastifyCors, {
  origin: true,
})

app.setErrorHandler(
  (error: FastifyError, _request: FastifyRequest, reply: FastifyReply) => {
    withErrors(error, reply)
  }
)

app.register(movieRoutes)

const start = async () => {
  try {
    await app.listen(port, '0.0.0.0')
    console.log(`Server listening on ${port}`)
  } catch (err) {
    console.log(err)
    throw err
  }
}

start()

export default app
