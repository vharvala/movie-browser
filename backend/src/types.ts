export interface OMDBSearchItem {
  Title: string
  Year: string
  imdbID: string
  Poster: string
}

export interface OMDBSearchResponse {
  Search: OMDBSearchItem[]
  Response: string
  totalResults: string
  Error?: string
}

export interface OMDBMovieDetails extends OMDBSearchItem {
  Plot: string
}

export interface OMDBMovieDetailsResponse extends OMDBMovieDetails {
  Response: string
  Error?: string
}

export interface NYTReviewItem {
  headline: string
  summary_short: string
  link: { url: string }
}

export interface NYTReviewResponse {
  status: string
  num_results: number
  results: NYTReviewItem[]
}

// FIXME: use shared types in frontend and backend for following types
export enum ReviewSource {
  NYT = 'New York Times',
}

export interface Review {
  title: string
  summary: string
  link: string
  source: ReviewSource
}

export interface MoviePreview {
  id: string
  title: string
  year: number
  poster?: string | null
}

export interface Movie extends MoviePreview {
  plot: string
  reviews: Review[]
}
