import got from 'got'
import config from '../config'

import { MoviePreview, OMDBSearchResponse } from '../types'
import { toMoviePreview } from '../utils/transform'

export default async (searchTerm: string): Promise<MoviePreview[]> => {
  if (!searchTerm && searchTerm.length === 0) {
    return []
  }

  const { body } = await got<OMDBSearchResponse>(config.omdb.url, {
    searchParams: {
      s: searchTerm,
      type: 'movie',
      apikey: config.omdb.apiKey,
    },
    responseType: 'json',
  })

  if (body.Error === 'Movie not found!') {
    return []
  }

  if (body.Error === 'Too many results.') {
    return []
  }

  if (body.Error) {
    throw new Error(body.Error)
  }

  return body.Search.map(toMoviePreview)
}
