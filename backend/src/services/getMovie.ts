import got from 'got'
import config from '../config'
import { NotFoundError } from '../errors'
import { Movie, NYTReviewResponse, OMDBMovieDetailsResponse } from '../types'
import { toMovie } from '../utils/transform'

export default async (id: string): Promise<Movie> => {
  const { body: omdbResult } = await got<OMDBMovieDetailsResponse>(
    config.omdb.url,
    {
      searchParams: {
        i: id,
        apiKey: config.omdb.apiKey,
      },
      responseType: 'json',
    }
  )

  if (omdbResult.Error === 'Incorrect IMDb ID.') {
    throw new NotFoundError(`Incorrect ID: ${id}`)
  }

  if (omdbResult.Error) {
    throw new Error(omdbResult.Error)
  }

  const { body: nytResult } = await got<NYTReviewResponse>(config.nyt.url, {
    searchParams: {
      query: omdbResult.Title,
      'api-key': config.nyt.apiKey,
    },
    responseType: 'json',
  })

  return toMovie(omdbResult, nytResult)
}
