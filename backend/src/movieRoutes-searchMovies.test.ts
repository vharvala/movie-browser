import fastify from './server'

describe('valid requests', () => {
  afterAll(() => {
    fastify.close()
  })

  test('search a movie with an valid search term', async (done) => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/?search=toy story 4',
    })

    expect(response.statusCode).toBe(200)
    expect(JSON.parse(response.payload)).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: 'tt1979376',
          title: 'Toy Story 4',
          poster:
            'https://m.media-amazon.com/images/M/MV5BMTYzMDM4NzkxOV5BMl5BanBnXkFtZTgwNzM1Mzg2NzM@._V1_SX300.jpg',
          year: 2019,
        }),
      ])
    )
    done()
  })

  test('search a movie with too many results', async (done) => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/?search=st',
    })

    expect(response.statusCode).toBe(200)
    expect(JSON.parse(response.payload)).toEqual(expect.arrayContaining([]))
    done()
  })

  test('search a movie with no results', async (done) => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/?search=dasfdsafasf',
    })

    expect(response.statusCode).toBe(200)
    expect(JSON.parse(response.payload)).toEqual(expect.arrayContaining([]))
    done()
  })
})

describe('invalid requests', () => {
  afterAll(() => {
    fastify.close()
  })

  test('call api without search term', async (done) => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/',
    })

    expect(response.statusCode).toBe(500)
    expect(JSON.parse(response.payload).message).toBe(
      "querystring should have required property 'search'"
    )
    done()
  })
})
