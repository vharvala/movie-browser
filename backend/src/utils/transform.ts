import {
  Movie,
  MoviePreview,
  NYTReviewResponse,
  OMDBMovieDetails,
  OMDBSearchItem,
  Review,
  ReviewSource,
} from '../types'

export const toMoviePreview = ({
  Title,
  imdbID,
  Poster,
  Year,
}: OMDBSearchItem): MoviePreview => ({
  id: imdbID,
  title: Title,
  poster: Poster !== 'N/A' ? Poster : null,
  year: Number.parseInt(Year),
})

export const toNytReviews = (response: NYTReviewResponse): Review[] => {
  if (response.status !== 'OK' || response.num_results === 0) {
    return []
  }

  const { summary_short, link, headline } = response.results[0]

  return [
    {
      title: headline,
      summary: summary_short,
      link: link.url,
      source: ReviewSource.NYT,
    },
  ]
}

export const toMovie = (
  omdbItem: OMDBMovieDetails,
  nytReviewItem: NYTReviewResponse
): Movie => ({
  ...toMoviePreview(omdbItem),
  plot: omdbItem.Plot,
  reviews: toNytReviews(nytReviewItem),
})
