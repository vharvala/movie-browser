import { FastifyError, FastifyInstance, FastifyPluginOptions } from 'fastify'
import S from 'fluent-schema'
import getMovie from './services/getMovie'
import searchMovies from './services/searchMovies'

const searchMoviesSchema = {
  querystring: S.object().prop('search', S.string().required()),
}

const getMovieSchema = {
  params: S.object().prop('id', S.string().required()),
}

export default (
  fastify: FastifyInstance,
  _opts: FastifyPluginOptions,
  done: (error?: FastifyError) => void
): void => {
  fastify.get<{ Querystring: { search: string } }>(
    '/',
    { schema: searchMoviesSchema },
    async (request) => searchMovies(request.query.search)
  )

  fastify.get<{ Params: { id: string } }>(
    '/:id',
    {
      schema: getMovieSchema,
    },
    async (request) => getMovie(request.params.id)
  )

  done()
}
