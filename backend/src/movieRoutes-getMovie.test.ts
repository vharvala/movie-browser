import fastify from './server'

describe('valid requests', () => {
  afterAll(() => {
    fastify.close()
  })

  test('get a movie with an valid id', async (done) => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/tt1979376',
    })

    expect(response.statusCode).toBe(200)
    expect(JSON.parse(response.payload)).toEqual(
      expect.objectContaining({
        id: 'tt1979376',
        title: 'Toy Story 4',
        poster:
          'https://m.media-amazon.com/images/M/MV5BMTYzMDM4NzkxOV5BMl5BanBnXkFtZTgwNzM1Mzg2NzM@._V1_SX300.jpg',
        year: 2019,
        plot:
          'When a new toy called "Forky" joins Woody and the gang, a road trip alongside old and new friends reveals how big the world can be for a toy.',
        reviews: [
          {
            title: '‘Toy Story 4’ Review: Playtime’s Over',
            summary:
              'In this latest entry in the long-running series, Woody and Buzz hit the road and cross paths with a scary, scarily unloved doll.',
            link:
              'http://www.nytimes.com/2019/06/20/movies/toy-story-4-review.html',
            source: 'New York Times',
          },
        ],
      })
    )
    done()
  })
})

describe('invalid requests', () => {
  afterAll(() => {
    fastify.close()
  })

  test('call api with faulty id', async (done) => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/tt197937',
    })

    expect(response.statusCode).toBe(404)
    expect(JSON.parse(response.payload).message).toBe(`Incorrect ID: tt197937`)
    done()
  })
})
