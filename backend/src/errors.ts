import { FastifyError, FastifyReply } from 'fastify'

enum ErrorStatusCode {
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500,
}

export class NotFoundError extends Error {}

export default (e: FastifyError, reply: FastifyReply): void => {
  let statusCode = ErrorStatusCode.INTERNAL_SERVER_ERROR
  let message = e.message

  if (e instanceof NotFoundError) {
    statusCode = ErrorStatusCode.NOT_FOUND
    message = e.message
  }

  reply.status(statusCode)
  reply.send({
    statusCode,
    message,
  })
}
