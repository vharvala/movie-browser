import * as dotenv from 'dotenv'

dotenv.config()

export default {
  app: {
    port: process.env.PORT ?? 1337,
  },
  omdb: {
    apiKey: process.env.OMDB_API_KEY,
    url: 'http://www.omdbapi.com',
  },
  nyt: {
    apiKey: process.env.NYT_API_KEY,
    url: 'https://api.nytimes.com/svc/movies/v2/reviews/search.json',
  },
}
