# movie-browser-backend

## Prerequisites

- node = 12.12
- eslint and prettier plugin installed to your editor

## Quick start guide

### Setup required env variables

```bash
cp .env.example .env
```

Add your OMDb and New York Times api keys to the file

### Run in production mode

```bash
yarn install
yarn start
```

### Run in development mode

```bash
yarn install
yarn dev
```

### Run API tests

```bash
yarn install
yarn test
```
