# movie-browser-frontend

## Prerequisites

- node = 12.12
- eslint and prettier plugin installed to your editor

## Quick start guide

### Setup required env variables

```bash
cp .env.example .env
```

Change (if needed) the backend url

### Run in production mode

```bash
yarn install
yarn build
yarn global add serve
serve -s build
```

[Go to browser](http://localhost:5000)

### Run in development mode

```bash
yarn install
yarn dev
```

[Go to browser](http://localhost:3000)

## Code formatting

Enable prettier formatting on save in your IDE to get the full dev experience.

Run to see formatting errors

```bash
yarn eslint
```

Fix formatting errors

```bash
yarn eslint --fix
```
