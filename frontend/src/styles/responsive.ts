const breakpoints = {
  desktop: 992,
  tablet: 768,
  phone: 376,
}

const getMediaQuery = (breakpoint: number): string =>
  `@media (min-width: ${breakpoint}px)`

export default {
  desktop: getMediaQuery(breakpoints.desktop),
  tablet: getMediaQuery(breakpoints.tablet),
  phone: getMediaQuery(breakpoints.phone),
}
