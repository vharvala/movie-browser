const neutral = {
  shade100: '#ffffff',
  shade200: '#f9fcfc',
  shade300: '#ecf0ef',
  shade400: '#c6cbcb',
  shade500: '#929897',
  shade600: '#494e4e',
  shade700: '#22272c',
  shade800: '#181a19',
  shade900: '#000000',
}

const primary = {
  shade200: '#9CADBC',
  shade300: '#597387',
  shade600: '#2a4158',
  shade800: '#384955',
}

export default {
  hover: {
    light: primary.shade800,
  },
  border: {
    light: neutral.shade300,
    neutral: primary.shade300,
  },
  text: {
    dark: neutral.shade700,
  },
  icon: {
    neutral: neutral.shade500,
    dark: neutral.shade400,
  },
  bg: {
    neutral: primary.shade600,
    dark: neutral.shade700,
  },
  link: {
    neutral: primary.shade200,
  },
}
