export default {
  size: {
    small: '0.875rem',
    regular: '1rem',
    big: '1.25rem',
  },
}
