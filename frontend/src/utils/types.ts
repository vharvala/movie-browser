// FIXME: use shared types in frontend and backend
export interface Review {
  title: string
  summary: string
  link: string
  source: string
}

export interface MoviePreview {
  id: string
  title: string
  year: number
  poster: string
}

export interface Movie extends MoviePreview {
  plot: string
  reviews: Review[]
}
