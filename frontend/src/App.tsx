import React, { FunctionComponent } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import MovieView from './components/views/MovieView/MovieView'
import SearchView from './components/views/SearchView/SearchView'
import { Layout } from './components/common'

const App: FunctionComponent = () => {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/:id">
            <MovieView />
          </Route>
          <Route path="/">
            <SearchView />
          </Route>
        </Switch>
      </Layout>
    </Router>
  )
}

export default App
