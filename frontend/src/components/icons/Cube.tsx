import { css } from '@emotion/core'
import React, { FunctionComponent } from 'react'

export interface Props {
  className?: string
}

const Cube: FunctionComponent<Props> = ({ className }) => {
  return (
    <svg
      className={className}
      css={css`
        height: 1rem;
        width: 1rem;
      `}
      fill="none"
      stroke="currentColor"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4"
      />
    </svg>
  )
}

export default React.memo(Cube)
