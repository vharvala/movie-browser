import { css } from '@emotion/core'
import React, { FunctionComponent } from 'react'

export interface Props {
  className?: string
}

const ExclamationCircle: FunctionComponent<Props> = ({ className }) => {
  return (
    <svg
      className={className}
      css={css`
        height: 1rem;
        width: 1rem;
      `}
      fill="none"
      stroke="currentColor"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
      />
    </svg>
  )
}

export default React.memo(ExclamationCircle)
