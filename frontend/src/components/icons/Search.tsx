import { css } from '@emotion/core'
import React, { FunctionComponent } from 'react'

export interface Props {
  className?: string
}

const Search: FunctionComponent<Props> = ({ className }) => {
  return (
    <svg
      className={className}
      css={css`
        height: 1rem;
        width: 1rem;
      `}
      fill="none"
      stroke="currentColor"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
      />
    </svg>
  )
}

export default React.memo(Search)
