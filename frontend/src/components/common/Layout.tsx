import React, { ReactNode, FunctionComponent } from 'react'
import { Global, css } from '@emotion/core'

import { colors, responsive, spacing } from '../../styles'

interface Props {
  children: ReactNode
}

const Layout: FunctionComponent<Props> = ({ children }) => {
  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
        width: 100%;
      `}
    >
      <Global styles={globalStyles} />
      <div css={contentStyles}>{children}</div>
    </div>
  )
}

const globalStyles = css`
  html,
  body {
    padding: 0;
    margin: 0;
    color: white;
    background: ${colors.bg.dark};
    min-height: 100%;
    font-family: 'Open Sans', sans-serif;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: 0;
    font-family: 'Work Sans', sans-serif;
    margin-bottom: 1em;
  }

  a {
    color: ${colors.link.neutral};
  }

  *,
  *::after,
  *::before {
    box-sizing: border-box;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-smoothing: antialiased;
  }
`

const contentStyles = css`
  padding: ${spacing.m};
  width: 100%;
  min-height: 100vh;

  ${responsive.tablet} {
    padding: ${spacing.l};
  }

  ${responsive.desktop} {
    padding: ${spacing.xl};
    max-width: 50rem;
  }
`

export default Layout
