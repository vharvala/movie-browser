import { css, keyframes } from '@emotion/core'
import React, { FunctionComponent } from 'react'

import { colors } from '../../styles'
import { Cube } from '../icons'

export interface Props {
  className?: string
}

const Loading: FunctionComponent<Props> = ({ className }) => {
  return <Cube className={className} css={styles} />
}

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const styles = css`
  color: ${colors.icon.neutral};
  height: 2rem;
  width: 2rem;
  animation-name: ${rotate};
  animation-duration: 5000ms;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
`

export default React.memo(Loading)
