import React, { FunctionComponent } from 'react'
import { css } from '@emotion/core'

import { colors, shape, spacing } from '../../styles'

export interface Props {
  className?: string
  url: string
}

const Poster: FunctionComponent<Props> = ({ className, url }) => {
  return (
    <div
      className={className}
      css={[
        styles,
        url
          ? css`
              background-image: url(${url});
              background-size: cover;
              background-repeat: no-repeat;
              background-position: 50% 30%;
            `
          : css`
              background-color: ${colors.bg.neutral};
            `,
      ]}
    />
  )
}

const styles = css`
  height: 100%;
  width: 100%;
  margin-right: ${spacing.m};
  border-radius: ${shape.radius.little};
`

export default React.memo(Poster)
