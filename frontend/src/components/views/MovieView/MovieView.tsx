import React, { FunctionComponent, useCallback } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import useFetch from 'use-http'
import { css } from '@emotion/core'

import { ArrowCircleLeft } from '../../icons'
import { spacing } from '../../../styles'
import { Movie as MovieType } from '../../../utils/types'
import { Loading } from '../../common'
import NotFound from './components/NotFound'
import Movie from './components/Movie'

export interface Props {
  className?: string
}

const MovieDetailsView: FunctionComponent<Props> = ({ className }) => {
  const { id } = useParams()

  const { loading, data, error } = useFetch<MovieType>(
    `${process.env.REACT_APP_API_URL}/${id}`,
    null,
    []
  )

  const history = useHistory()

  const onBack = useCallback(() => history.push('/'), [history])

  return (
    <div className={className} css={styles}>
      {error ? (
        <NotFound />
      ) : loading ? (
        <Loading />
      ) : (
        <div>
          <div css={backStyles} onClick={onBack}>
            <ArrowCircleLeft
              css={css`
                width: 2rem;
                height: 2rem;
              `}
            />
          </div>
          <Movie movie={data} />
        </div>
      )}
    </div>
  )
}

const styles = css`
  display: flex;
  justify-content: center;
`

const backStyles = css`
  width: 100%;
  margin-bottom: ${spacing.m};
  cursor: pointer;
`

export default React.memo(MovieDetailsView)
