import { css } from '@emotion/core'
import React, { FunctionComponent } from 'react'

import { spacing } from '../../../../styles'
import ExclamationCircle from '../../../icons/ExclamationCircle'

export interface Props {
  className?: string
}

const NotFound: FunctionComponent<Props> = ({ className }) => {
  return (
    <div
      className={className}
      css={css`
        width: 100%;
        display: flex;
        align-items: center;
      `}
    >
      <ExclamationCircle
        css={css`
          width: 3rem;
          height: 3rem;
        `}
      />
      <h2
        css={css`
          margin-bottom: 0;
          margin-left: ${spacing.s};
        `}
      >
        Movie not found
      </h2>
    </div>
  )
}

export default React.memo(NotFound)
