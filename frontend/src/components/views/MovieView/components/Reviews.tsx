import React, { FunctionComponent } from 'react'
import { css } from '@emotion/core'

import { Review } from '../../../../utils/types'
import { colors, font } from '../../../../styles'

export interface Props {
  className?: string
  reviews: Review[]
}

const Reviews: FunctionComponent<Props> = ({ className, reviews }) => (
  <div className={className}>
    {reviews.length === 0 && 'No reviews found.'}
    {reviews.map(({ title: reviewTitle, source, summary, link }) => (
      <div
        key={`${source}_${reviewTitle}`}
        css={css`
          & + & {
            border-top: ${colors.border.light};
          }
        `}
      >
        <h4>
          {source}:&nbsp;
          {reviewTitle}&nbsp;
        </h4>
        <q>{summary}</q>
        <p>
          <a
            href={link}
            target="_blank"
            rel="noopener noreferrer"
            css={css`
              font-size: ${font.size.small};
            `}
          >
            Read more...
          </a>
        </p>
      </div>
    ))}
  </div>
)

export default React.memo(Reviews)
