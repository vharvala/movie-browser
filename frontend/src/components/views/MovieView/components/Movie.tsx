import React, { FunctionComponent } from 'react'
import { css } from '@emotion/core'

import { Movie as MovieType } from '../../../../utils/types'
import { colors, responsive, spacing } from '../../../../styles'
import Reviews from './Reviews'
import { Poster } from '../../../common'

export interface Props {
  className?: string
  movie: MovieType
}

const Movie: FunctionComponent<Props> = ({ className, movie }) => {
  const { poster, title, year, plot, reviews } = movie

  return (
    <div className={className}>
      <section css={detailsStyles}>
        <Poster url={poster} css={posterStyles} />
        <div css={detailTextStyles}>
          <h2>
            {title} ({year})
          </h2>
          <p>{plot}</p>
        </div>
      </section>
      <hr css={lineStyles} />
      <section>
        <h3>Reviews</h3>
        <Reviews reviews={reviews} />
      </section>
    </div>
  )
}

const posterStyles = css`
  width: 100%;
  height: 20rem;

  ${responsive.desktop} {
    height: 25rem;
  }
`

const detailsStyles = css`
  ${responsive.tablet} {
    display: grid;
    grid-template-columns: 35% 65%;
    grid-template-rows: 1fr;
    grid-column-gap: ${spacing.m};
  }
`

const detailTextStyles = css`
  margin-top: ${spacing.l};
  ${responsive.tablet} {
    margin-top: 0;
  }
`

const lineStyles = css`
  margin: ${spacing.l} 0;
  border: 0;
  border-top: 1px solid ${colors.border.light};
  opacity: 0.5;
`

export default React.memo(Movie)
