import React, { FunctionComponent, useCallback, useState } from 'react'
import useFetch from 'use-http'
import debounce from 'lodash.debounce'
import { css } from '@emotion/core'

import { MoviePreview } from '../../../utils/types'
import { spacing } from '../../../styles'
import { Loading } from '../../common'
import MovieList from './components/MovieList'
import SearchBar from './components/SearchBar'

export interface Props {
  className?: string
}

const SearchView: FunctionComponent<Props> = ({ className }) => {
  const [searchTerm, setSearchTerm] = useState<string>('')

  const { get, loading, data = [] } = useFetch<MoviePreview[]>(
    `${process.env.REACT_APP_API_URL}`
  )

  const fetchData = useCallback(
    debounce((newSearchTerm) => get(`?search=${newSearchTerm}`), 150),
    []
  )

  const onChange = useCallback(
    (newSearchTerm) => {
      setSearchTerm(newSearchTerm)
      fetchData(newSearchTerm)
    },
    [fetchData]
  )

  const noResults = data.length === 0 && searchTerm.length > 0 && !loading

  return (
    <div className={className} css={styles}>
      <SearchBar onChange={onChange} value={searchTerm} />
      <div css={searchResultStyles} className="search-results">
        {loading ? (
          <Loading />
        ) : noResults ? (
          <h3>No or too many results. Try again!</h3>
        ) : (
          <MovieList className="movies" movies={data} />
        )}
      </div>
    </div>
  )
}

const styles = css`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  min-height: 100vh;
`

const searchResultStyles = css`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: ${spacing.xl};
`

export default React.memo(SearchView)
