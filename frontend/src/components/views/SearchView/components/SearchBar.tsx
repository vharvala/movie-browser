import React, { FunctionComponent } from 'react'
import { css } from '@emotion/core'

import { colors, font, responsive, shape, spacing } from '../../../../styles'
import { Search } from '../../../icons'

export interface Props {
  className?: string
  onChange: (value: string) => void
  value?: string
}

const SearchBar: FunctionComponent<Props> = ({
  className,
  onChange,
  value,
}) => {
  const onChangeFunc = (event): void => {
    onChange(event.target.value)
  }

  return (
    <div className={className} css={styles}>
      <div css={iconWrapperStyles}>
        <Search css={iconStyles} />
      </div>
      <input
        placeholder="Search for movie"
        onChange={onChangeFunc}
        value={value}
        css={inputStyles}
      />
    </div>
  )
}

const styles = css`
  position: relative;
  width: 100%;
  border: 1px solid ${colors.border.neutral};
  border-radius: ${shape.radius.little};
  background-color: ${colors.bg.neutral};
`

const iconWrapperStyles = css`
  position: absolute;
  display: flex;
  align-items: center;
  padding-left: ${spacing.m};
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  pointer-events: none;
`

const iconStyles = css`
  height: 1.275rem;
  width: 1.275rem;
  color: ${colors.icon.dark};
`

const inputStyles = css`
  display: block;
  width: 100%;
  padding: ${spacing.m} ${spacing.l};
  padding-left: ${spacing.xl};
  color: white;
  background-color: inherit;
  border: inherit;
  border-radius: inherit;

  &::placeholder {
    color: white;
    opacity: 0.5;
  }

  ${responsive.desktop} {
    font-size: ${font.size.big};
  }
`

export default React.memo(SearchBar)
