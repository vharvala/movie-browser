import React, { FunctionComponent } from 'react'
import { css } from '@emotion/core'

import { spacing } from '../../../../styles'
import { MoviePreview as MoviePreviewType } from '../../../../utils/types'
import MoviePreview from './MoviePreview'

export interface Props {
  className?: string
  movies?: MoviePreviewType[]
}

const MovieList: FunctionComponent<Props> = ({ className, movies = [] }) => {
  return (
    <div
      className={className}
      css={css`
        width: 100%;
      `}
    >
      {movies.map((movie) => (
        <MoviePreview
          key={movie.id}
          movie={movie}
          css={css`
            & + & {
              margin-top: ${spacing.m};
            }
          `}
        />
      ))}
    </div>
  )
}

export default React.memo(MovieList)
