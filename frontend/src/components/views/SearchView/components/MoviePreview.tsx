import React, { FunctionComponent } from 'react'
import { Link } from 'react-router-dom'
import { css } from '@emotion/core'

import { MoviePreview as MoviePreviewType } from '../../../../utils/types'
import { colors, shape, spacing } from '../../../../styles'
import { Poster } from '../../../common'

export interface Props {
  className?: string
  movie: MoviePreviewType
}

const MoviePreview: FunctionComponent<Props> = ({ className, movie }) => {
  const { id, title, poster, year } = movie

  return (
    <Link className={className} to={`/${id}`} css={linkStyles}>
      <Poster
        url={poster}
        css={css`
          height: 4.5rem;
        `}
      />
      <h3
        css={css`
          margin-left: ${spacing.m};
          margin-bottom: 0;
        `}
      >
        {title} ({year})
      </h3>
    </Link>
  )
}

const linkStyles = css`
  display: grid;
  grid-template-columns: 5rem 1fr;
  grid-template-rows: 1fr;
  align-items: center;
  color: white;
  width: 100%;
  padding-right: ${spacing.m};
  border-radius: ${shape.radius.little};
  text-decoration: none;

  &:hover {
    background-color: ${colors.hover.light};
    cursor: pointer;
  }
`

export default React.memo(MoviePreview)
