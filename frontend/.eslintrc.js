module.exports = {
  extends: ['airbnb-typescript-prettier', "plugin:cypress/recommended"],
  plugins: ['cypress'],
  "env": {
    "cypress/globals": true
  },
  rules: {
    "no-nested-ternary": "off",
    "@typescript-eslint/no-use-before-define": "off",
    "jsx-a11y/click-events-have-key-events": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "import/order": [
      "error",
      {
        "groups": [
          ["builtin", "external"],
          ["parent", "sibling", "index"]
        ],
        "newlines-between": "always"
      }
    ],
  }
}
