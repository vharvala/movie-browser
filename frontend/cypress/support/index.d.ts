/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    updateViewport(
      value: Cypress.ViewportPreset | [number, number]
    ): Chainable<Element>
  }
}
