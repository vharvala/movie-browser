/// <reference types="cypress" />
/// <reference types="../../support" />

type Size = Cypress.ViewportPreset | [number, number]
const sizes: Size[] = ['iphone-6', 'ipad-2', [1024, 768]]

context('Search', () => {
  sizes.forEach((size) => {
    describe(`Search with viewport ${size}`, () => {
      beforeEach(() => {
        cy.visit('/')
        cy.updateViewport(size)
      })

      it(`Should find search results`, () => {
        cy.get('input').type('Indiana')
        cy.get('.movies')
          .children()
          .first()
          .should(
            'contain',
            'Indiana Jones and the Raiders of the Lost Ark (1981)'
          )
      })

      it(`Should give a message if too many results`, () => {
        cy.get('input').type('In')
        cy.get('.search-results')
          .children()
          .first()
          .should('contain', 'No or too many results. Try again!')
      })
    })
  })
})
